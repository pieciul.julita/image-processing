package bio;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class KMMAlgorithm {
    private Image image;
    private int width;
    private int height;

    private static int[][] WEIGHT_MASK = {
            {128, 1, 2},
            {64, 0, 4},
            {32, 16, 8}
    };

    private static int[] NEIGHBOURS = {
            3, 6, 12, 24, 48, 96, 129, 192, //2 neighbours
            7, 14, 28, 56, 112, 224, 193, 131, //3 neighbours
            15, 30, 60, 120, 240, 225, 195, 135//4 neighbours
    };

    private static int[] TO_CLEAR = {
            3, 5, 7, 12, 13, 14, 15, 20,
            21, 22, 23, 28, 29, 30, 31, 48,
            52, 53, 54, 55, 56, 60, 61, 62,
            63, 65, 67, 69, 71, 77, 79, 80,
            81, 83, 84, 85, 86, 87, 88, 89,
            91, 92, 93, 94, 95, 97, 99, 101,
            103, 109, 111, 112, 113, 115, 116, 117,
            118, 119, 120, 121, 123, 124, 125, 126,
            127, 131, 133, 135, 141, 143, 149, 151,
            157, 159, 181, 183, 189, 191, 192, 193,
            195, 197, 199, 205, 207, 208, 209, 211,
            212, 213, 214, 215, 216, 217, 219, 220,
            221, 222, 223, 224, 225, 227, 229, 231,
            237, 239, 240, 241, 243, 244, 245, 246,
            247, 248, 251, 252, 253, 254, 255
    };

    public int[][] processKMM(Image image) {
        this.image = image;
        this.width = (int) image.getWidth();
        this.height = (int) image.getHeight();
        //Add 0 for white, 1 for black
        int[][] imageArray = setBaseBlackWhiteValues();

        boolean stopped = false;
        while (!stopped) {
            stopped = true;

            //Set 2 and 3 for border
            imageArray = setBorderValues(imageArray);

            //Set 4
            imageArray = setCorners(imageArray);

            imageArray = clearFours(imageArray);

            for (int i = 1; i < width - 1; i++) {
                for (int j = 1; j < height - 1; j++) {
                    if (imageArray[i][j] == 2) {
                        if (checkCanClear(i, j, imageArray)) {
                            imageArray[i][j] = 0;
                            stopped = false;
                        } else {
                            imageArray[i][j] = 1;
                        }
                    }
                }
            }
            for (int i = 1; i < width - 1; i++) {
                for (int j = 1; j < height - 1; j++) {
                    if (imageArray[i][j] == 3) {
                        if (checkCanClear(i, j, imageArray)) {
                            imageArray[i][j] = 0;
                            stopped = false;
                        } else {
                            imageArray[i][j] = 1;
                        }
                    }
                }
            }
        }

        return imageArray;
    }

    private int[][] clearFours(int[][] values) {
        for (int i = 1; i < width - 1; i++) {
            for (int j = 1; j < height - 1; j++) {
                if (values[i][j] == 4) {
                    values[i][j] = 0;
                }

            }
        }

        return values;
    }


    private int[][] setBaseBlackWhiteValues() {

        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                result[i][j] = (image.getPixelReader().getColor(i, j).equals(Color.BLACK)) ? 1 : 0;
            }
        }

        return result;
    }

    private int[][] setBorderValues(int[][] values) {

        for (int i = 1; i < width - 1; i++) {
            for (int j = 1; j < height - 1; j++) {
                if (values[i][j] == 0) continue;
                if (values[i][j - 1] == 0 || values[i][j + 1] == 0 || values[i + 1][j] == 0 || values[i - 1][j] == 0)
                    values[i][j] = 2;

                else if (values[i + 1][j + 1] == 0 || values[i + 1][j - 1] == 0 || values[i - 1][j + 1] == 0 || values[i - 1][j - 1] == 0)
                    values[i][j] = 3;
            }
        }


        return values;
    }

    private int[][] setCorners(int[][] values) {
        for (int i = 1; i < width - 1; i++) {
            for (int j = 1; j < height - 1; j++) {
                if (values[i][j] == 0) continue;

                if (checkValidNeighbourhood(i, j, values)) values[i][j] = 4;
            }
        }
        return values;
    }

    private int sumNeighbourhood(int x, int y, int[][] values) {

        int sum = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (x + i < 0 || x + i >= width || y + j < 0 || y + j >= height) continue;
                if (values[x + i][y + j] != 0)
                    sum += WEIGHT_MASK[i + 1][j + 1];
            }
        }
        System.out.println(sum);
        return sum;
    }

    private boolean checkValidNeighbourhood(int x, int y, int[][] values) {

        boolean exist = false;
        int sum = sumNeighbourhood(x, y, values);
        for (int i = 0; i < NEIGHBOURS.length; i++) {
            if (NEIGHBOURS[i] == sum) exist = true;
        }

        return exist;
    }

    private boolean checkCanClear(int x, int y, int[][] values) {
        boolean exist = false;
        int sum = sumNeighbourhood(x, y, values);
        for (int i = 0; i < TO_CLEAR.length; i++) {
            if (TO_CLEAR[i] == sum) exist = true;
        }

        return exist;
    }

}
